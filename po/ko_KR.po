# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# 박정규(Jung-Kyu Park) <bagjunggyu@gmail.com>, 2016
msgid ""
msgstr ""
"Project-Id-Version: Ubuntu MATE Welcome\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-06-14 18:20+0100\n"
"PO-Revision-Date: 2016-06-06 16:10+0000\n"
"Last-Translator: Martin Wimpress <code@flexion.org>\n"
"Language-Team: Korean (Korea) (http://www.transifex.com/ubuntu-mate/ubuntu-"
"mate-welcome/language/ko_KR/)\n"
"Language: ko_KR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: ubuntu-mate-welcome:71
msgid ""
"Software changes are in progress. Please allow them to complete before "
"closing Welcome."
msgstr ""
"소프트웨어 변경이 진행 중입니다. 환영 인사를 닫기 전에 마칠 수 있도록 해주세"
"요."

#: ubuntu-mate-welcome:72 ubuntu-mate-welcome:2489
msgid "OK"
msgstr "확인"

#: ubuntu-mate-welcome:159 ubuntu-mate-welcome:169
msgid "Successfully performed fix."
msgstr ""

#: ubuntu-mate-welcome:159
msgid "Any previously incomplete installations have been finished."
msgstr ""

#: ubuntu-mate-welcome:162 ubuntu-mate-welcome:172
msgid "Failed to perform fix."
msgstr ""

#: ubuntu-mate-welcome:162
msgid "Errors occurred while finishing an incomplete installation."
msgstr ""

#: ubuntu-mate-welcome:169
msgid "Packages with broken dependencies have been resolved."
msgstr ""

#: ubuntu-mate-welcome:172
msgid "Packages may still have broken dependencies."
msgstr ""

#: ubuntu-mate-welcome:181
msgid "Successfully updated cache."
msgstr ""

#: ubuntu-mate-welcome:181
msgid "Software is now ready to install."
msgstr ""

#: ubuntu-mate-welcome:184
msgid "Failed to update cache."
msgstr ""

#: ubuntu-mate-welcome:184
msgid "There may be a problem with your repository configuration."
msgstr ""

#: ubuntu-mate-welcome:240
msgid "Welcome will stay up-to-date."
msgstr ""

#: ubuntu-mate-welcome:241
#, fuzzy
msgid ""
"Welcome and the Software Boutique are set to receive the latest updates."
msgstr "이 애플리케이션은 최신 업데이트를 수신하도록 설정되어 있습니다."

#: ubuntu-mate-welcome:247
msgid "Everything is up-to-date"
msgstr ""

#: ubuntu-mate-welcome:248
msgid "All packages have been upgraded to the latest versions."
msgstr ""

#: ubuntu-mate-welcome:253
#, fuzzy
msgid "Installed"
msgstr "설치"

#: ubuntu-mate-welcome:254
#, fuzzy
msgid "The application is now ready to use."
msgstr "이 애플리케이션은 최신 업데이트를 수신하도록 설정되어 있습니다."

#: ubuntu-mate-welcome:256
#, fuzzy
msgid "was not installed."
msgstr "재설치"

#: ubuntu-mate-welcome:257 ubuntu-mate-welcome:267 ubuntu-mate-welcome:298
#: ubuntu-mate-welcome:308
msgid "The operation was cancelled."
msgstr ""

#: ubuntu-mate-welcome:259
#, fuzzy
msgid "failed to install"
msgstr "재설치"

#: ubuntu-mate-welcome:260
msgid "There was a problem installing this application."
msgstr ""

#: ubuntu-mate-welcome:263 ubuntu-mate-welcome:827
msgid "Removed"
msgstr ""

#: ubuntu-mate-welcome:264
#, fuzzy
msgid "The application has been uninstalled."
msgstr "루트 파티션의 크기가 조정되었습니다."

#: ubuntu-mate-welcome:266
#, fuzzy
msgid "was not removed."
msgstr "제거"

#: ubuntu-mate-welcome:269
msgid "failed to remove"
msgstr ""

#: ubuntu-mate-welcome:270
msgid "A problem is preventing this application from being removed."
msgstr ""

#: ubuntu-mate-welcome:273
#, fuzzy
msgid "Upgraded"
msgstr "업그레이드"

#: ubuntu-mate-welcome:274
#, fuzzy
msgid "This application is set to use the latest version."
msgstr "이 애플리케이션은 최신 업데이트를 수신하도록 설정되어 있습니다."

#: ubuntu-mate-welcome:276
#, fuzzy
msgid "was not upgraded."
msgstr "업그레이드"

#: ubuntu-mate-welcome:277
#, fuzzy
msgid "The application will continue to use the stable version."
msgstr "이 애플리케이션은 최신 업데이트를 수신하도록 설정되어 있습니다."

#: ubuntu-mate-welcome:279
msgid "failed to upgrade"
msgstr ""

#: ubuntu-mate-welcome:280
#, fuzzy
msgid "A problem is preventing this application from being upgraded."
msgstr "애플리케이션들이 업데이트되는 동안 잠시 기다려주세요..."

#: ubuntu-mate-welcome:294
msgid "applications have been installed"
msgstr ""

#: ubuntu-mate-welcome:295
msgid "Your newly installed applications are ready to use."
msgstr ""

#: ubuntu-mate-welcome:297
msgid "Some applications were not installed."
msgstr ""

#: ubuntu-mate-welcome:300
msgid "Some applications failed to install."
msgstr ""

#: ubuntu-mate-welcome:301 ubuntu-mate-welcome:311
msgid "Software Boutique encountered a problem with one of the applications."
msgstr ""

#: ubuntu-mate-welcome:304
#, fuzzy
msgid "applications have been removed"
msgstr "루트 파티션의 크기가 조정되었습니다."

#: ubuntu-mate-welcome:305
msgid "These applications are no longer present on your system."
msgstr ""

#: ubuntu-mate-welcome:307
msgid "Some applications were not removed."
msgstr ""

#: ubuntu-mate-welcome:310
msgid "Some applications failed to remove."
msgstr ""

#: ubuntu-mate-welcome:477
msgid "Blu-ray AACS database install succeeded"
msgstr "블루-레이  AACS 데이터베이스 설치 성공"

#: ubuntu-mate-welcome:478
msgid "Successfully installed the Blu-ray AACS database."
msgstr "블루-레이 AACS 데이터베이스 제대로 설치함."

#: ubuntu-mate-welcome:478
msgid "Installation of the Blu-ray AACS database was successful."
msgstr "블루-레이 AACS 데이터베이스 설치를 성공했습니다."

#: ubuntu-mate-welcome:481
msgid "Blu-ray AACS database install failed"
msgstr "블루-레이 AACS 데이터베이스 설치 실패"

#: ubuntu-mate-welcome:482
msgid "Failed to install the Blu-ray AACS database."
msgstr "블루-레이 AACS 데이터베이스 설치 실패함."

#: ubuntu-mate-welcome:482
msgid "Installation of the Blu-ray AACS database failed."
msgstr "블루-레이 AACS 데이터베이스 설치를 실패했습니다."

#: ubuntu-mate-welcome:767
msgid "Welcome"
msgstr "환영합니다"

#: ubuntu-mate-welcome:768
msgid "Software Boutique"
msgstr "소프트웨어 부띠끄"

#: ubuntu-mate-welcome:769
msgid "Close"
msgstr "닫기"

#: ubuntu-mate-welcome:770
msgid "Cancel"
msgstr ""

#: ubuntu-mate-welcome:773
msgid "Set to retrieve the latest software listings."
msgstr "검색 할 최신 소프트웨어 목록을 설정합니다."

#: ubuntu-mate-welcome:774
msgid "Retrieve the latest software listings."
msgstr "최신 소프트웨어 목록을 검색합니다."

#: ubuntu-mate-welcome:775
msgid "Please wait while the application is being updated..."
msgstr "애플리케이션들이 업데이트되는 동안 잠시 기다려주세요..."

#: ubuntu-mate-welcome:776
msgid "Version:"
msgstr "버전:"

#: ubuntu-mate-welcome:779
msgid "This application is set to receive the latest updates."
msgstr "이 애플리케이션은 최신 업데이트를 수신하도록 설정되어 있습니다."

#: ubuntu-mate-welcome:780
msgid "Alternative to:"
msgstr "대안:"

#: ubuntu-mate-welcome:781
msgid "Hide"
msgstr "숨김"

#: ubuntu-mate-welcome:782
msgid "Show"
msgstr "표시"

#: ubuntu-mate-welcome:783
msgid "Install"
msgstr "설치"

#: ubuntu-mate-welcome:784
msgid "Reinstall"
msgstr "재설치"

#: ubuntu-mate-welcome:785
msgid "Remove"
msgstr "제거"

#: ubuntu-mate-welcome:786
msgid "Upgrade"
msgstr "업그레이드"

#: ubuntu-mate-welcome:787
msgid "Launch"
msgstr "실행"

#: ubuntu-mate-welcome:788
msgid "License"
msgstr "라이선스"

#: ubuntu-mate-welcome:789
msgid "Platform"
msgstr "플랫폼"

#: ubuntu-mate-welcome:790
msgid "Category"
msgstr "카테고리"

#: ubuntu-mate-welcome:791
msgid "Website"
msgstr "웹사이트"

#: ubuntu-mate-welcome:792
msgid "Screenshot"
msgstr "스크린샷"

#: ubuntu-mate-welcome:793
msgid "Source"
msgstr "소스"

#: ubuntu-mate-welcome:794
msgid "Canonical Partner Repository"
msgstr "캐노니컬 파트너 저장소"

#: ubuntu-mate-welcome:795
#, fuzzy
msgid "Ubuntu Multiverse Repository"
msgstr "우분투 저장소"

#: ubuntu-mate-welcome:796
msgid "Ubuntu Repository"
msgstr "우분투 저장소"

#: ubuntu-mate-welcome:797
msgid "Unknown"
msgstr "모름"

#: ubuntu-mate-welcome:798
msgid "Undo Changes"
msgstr ""

#: ubuntu-mate-welcome:801
msgid "Installing..."
msgstr "설지 중..."

#: ubuntu-mate-welcome:802
msgid "Removing..."
msgstr "제거 중..."

#: ubuntu-mate-welcome:803
msgid "Upgrading..."
msgstr "업그레이드 중..."

#: ubuntu-mate-welcome:806
msgid "Accessories"
msgstr "액세서리"

#: ubuntu-mate-welcome:807
msgid "Education"
msgstr "교육"

#: ubuntu-mate-welcome:808
msgid "Games"
msgstr "게임"

#: ubuntu-mate-welcome:809
msgid "Graphics"
msgstr "그래픽"

#: ubuntu-mate-welcome:810
msgid "Internet"
msgstr "인터넷"

#: ubuntu-mate-welcome:811
msgid "Office"
msgstr "오피스"

#: ubuntu-mate-welcome:812
msgid "Programming"
msgstr "프로그래밍"

#: ubuntu-mate-welcome:813
msgid "Sound & Video"
msgstr "사운드 & 비디오"

#: ubuntu-mate-welcome:814
msgid "System Tools"
msgstr "시스템 도구"

#: ubuntu-mate-welcome:815
msgid "Universal Access"
msgstr "보편적인 기능"

#: ubuntu-mate-welcome:816
msgid "Server One-Click Installation"
msgstr ""

#: ubuntu-mate-welcome:817
msgid "Miscellaneous"
msgstr ""

#: ubuntu-mate-welcome:820
msgid "Search"
msgstr ""

#: ubuntu-mate-welcome:821
msgid "Please enter a keyword to begin."
msgstr ""

#: ubuntu-mate-welcome:822
msgid "Please enter at least 3 characters."
msgstr ""

#: ubuntu-mate-welcome:825
msgid "Added"
msgstr ""

#: ubuntu-mate-welcome:826
msgid "Fixed"
msgstr ""

#: ubuntu-mate-welcome:830
msgid "Queued for installation."
msgstr ""

#: ubuntu-mate-welcome:831
msgid "Queued for removal."
msgstr ""

#: ubuntu-mate-welcome:832
msgid "Preparing to remove:"
msgstr ""

#: ubuntu-mate-welcome:833
msgid "Preparing to install:"
msgstr ""

#: ubuntu-mate-welcome:834
#, fuzzy
msgid "Removing:"
msgstr "제거 중..."

#: ubuntu-mate-welcome:835
#, fuzzy
msgid "Installing:"
msgstr "설지 중..."

#: ubuntu-mate-welcome:836
msgid "Updating cache..."
msgstr ""

#: ubuntu-mate-welcome:837
msgid "Verifying software changes..."
msgstr ""

#: ubuntu-mate-welcome:838
msgid "Successfully Installed"
msgstr ""

#: ubuntu-mate-welcome:839
msgid "Failed to Install"
msgstr ""

#: ubuntu-mate-welcome:840
msgid "Successfully Removed"
msgstr ""

#: ubuntu-mate-welcome:841
msgid "Failed to Remove"
msgstr ""

#: ubuntu-mate-welcome:842
#, fuzzy
msgid "To be installed"
msgstr "재설치"

#: ubuntu-mate-welcome:843
#, fuzzy
msgid "To be removed"
msgstr "제거"

#: ubuntu-mate-welcome:1051
msgid "Servers"
msgstr "서버"

#: ubuntu-mate-welcome:1085
msgid "Jan"
msgstr "1월"

#: ubuntu-mate-welcome:1086
msgid "Feb"
msgstr "2월"

#: ubuntu-mate-welcome:1087
msgid "Mar"
msgstr "3월"

#: ubuntu-mate-welcome:1088
msgid "Apr"
msgstr "4월"

#: ubuntu-mate-welcome:1089
msgid "May"
msgstr "5월"

#: ubuntu-mate-welcome:1090
msgid "Jun"
msgstr "6월"

#: ubuntu-mate-welcome:1091
msgid "Jul"
msgstr "7월"

#: ubuntu-mate-welcome:1092
msgid "Aug"
msgstr "8월"

#: ubuntu-mate-welcome:1093
msgid "Sep"
msgstr "9월"

#: ubuntu-mate-welcome:1094
msgid "Oct"
msgstr "10월"

#: ubuntu-mate-welcome:1095
msgid "Nov"
msgstr "11월"

#: ubuntu-mate-welcome:1096
msgid "Dec"
msgstr "12월"

#: ubuntu-mate-welcome:1616
msgid "MB"
msgstr "MB"

#: ubuntu-mate-welcome:1617
msgid "MiB"
msgstr "MiB"

#: ubuntu-mate-welcome:1618
msgid "GB"
msgstr "GB"

#: ubuntu-mate-welcome:1619
msgid "GiB"
msgstr "GiB"

#: ubuntu-mate-welcome:1627
msgid "Could not gather data."
msgstr "데이터를 수집 할 수 없습니다."

#: ubuntu-mate-welcome:1926
msgid "Raspberry Pi Partition Resize"
msgstr "라즈베리 파이 파티션 크기 조정"

#: ubuntu-mate-welcome:1943
msgid "Root partition has been resized."
msgstr "루트 파티션의 크기가 조정되었습니다."

#: ubuntu-mate-welcome:1943
msgid "The filesystem will be enlarged upon the next reboot."
msgstr "파일 시스템이 다음 부팅시 확대됩니다."

#: ubuntu-mate-welcome:1949 ubuntu-mate-welcome:1951 ubuntu-mate-welcome:1953
#: ubuntu-mate-welcome:1955
msgid "Don't know how to expand."
msgstr "확장하는 방법을 모르겠어요."

#: ubuntu-mate-welcome:1949
msgid "does not exist or is not a symlink."
msgstr "존재하지 않거나 심볼릭 링크가 아닙니다."

#: ubuntu-mate-welcome:1951
msgid "is not an SD card."
msgstr "SD 카드가 아닙니다."

#: ubuntu-mate-welcome:1953
msgid "Your partition layout is not currently supported by this tool."
msgstr "현재 이 도구가 지원하지 않는 파티션 레이아웃입니다."

#: ubuntu-mate-welcome:1955
msgid "is not the last partition."
msgstr "마지막 파티션이 아닙니다."

#: ubuntu-mate-welcome:1957
msgid "Failed to run resize script."
msgstr ""

#: ubuntu-mate-welcome:1957
msgid "The returned error code is:"
msgstr ""

#: ubuntu-mate-welcome:2160
msgid "Open Source"
msgstr "오픈 소스"

#: ubuntu-mate-welcome:2162
msgid "Proprietary"
msgstr "소유권"

#: ubuntu-mate-welcome:2278
msgid ""
"Sorry, Welcome could not feature any software for this category that is "
"compatible on this system."
msgstr ""
"죄송합니다, 웰컴이 시스템과 호환 가능한 이 카테고리의 어느 소프트웨어도 특징 "
"지을 수 없습니다."

#: ubuntu-mate-welcome:2490
msgid ""
"An error occurred while launching PROGRAM_NAME. Please consider re-"
"installing the application."
msgstr ""
"PROGRAM_NAME 실행하는 동안 오류가 생겼습니다. 애플리케이션을 재설치를 고려하"
"시기 바랍니다."

#: ubuntu-mate-welcome:2491
msgid "Command:"
msgstr "명령어:"

#: ubuntu-mate-welcome:2573
msgid "Uses a new or updated source."
msgstr ""

#: ubuntu-mate-welcome:2575
msgid "Now works for various versions of Ubuntu."
msgstr ""

#: ubuntu-mate-welcome:2577
msgid "Now a snappy package."
msgstr ""

#: ubuntu-mate-welcome:2579
msgid "General maintenance."
msgstr ""

#: ubuntu-mate-welcome:2581
msgid "Broken or problematic source."
msgstr ""

#: ubuntu-mate-welcome:2583
msgid "Not yet available for some releases."
msgstr ""

#: ubuntu-mate-welcome:2585
msgid "No longer works for some releases."
msgstr ""

#: ubuntu-mate-welcome:2587
msgid "Not suitable for production machines."
msgstr ""

#: ubuntu-mate-welcome:2589
msgid "Requires further testing."
msgstr ""

#: ubuntu-mate-welcome:2591
msgid "Does not meet our standards to be featured."
msgstr ""

#: ubuntu-mate-welcome:2762
msgid "applications found."
msgstr ""

#: ubuntu-mate-welcome:2766
msgid "proprietary applications are hidden."
msgstr ""

#, fuzzy
#~ msgid "Removing software..."
#~ msgstr "제거 중..."

#, fuzzy
#~ msgid "Installing software..."
#~ msgstr "설지 중..."

#, fuzzy
#~ msgid "Installation of "
#~ msgstr "설지 중..."

#, fuzzy
#~ msgid "Upgrade of "
#~ msgstr "업그레이드"

#~ msgid "Skip"
#~ msgstr "넘기기"
